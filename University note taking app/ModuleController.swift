//
//  ModuleController.swift
//  University note taking app
//
//  Created by Mohammad Yusuf on 28/11/2016.
//  Copyright © 2016 Mohammad Yusuf. All rights reserved.
//

import UIKit
//A module contoller class has been created with the superclass UITableViewController
class ModuleController: UITableViewController {
    //Create a variable called moduleID
    public var moduleID:String?
    //create a variable called modules
    var modules:[String] = []
    //This was first 'ctrl dragged' then added some code to perform the action in this case it will add modules and save it.
    @IBAction func AddModule(_ sender: AnyObject) {
        print("Module added")
        let alert = UIAlertController(title: "New Module", message: "Enter Module Below", preferredStyle: .alert)
        alert.addTextField(configurationHandler: nil)
        alert.addAction(UIAlertAction(title: "Add", style: .default, handler: { (action) in
            if let textFields = alert.textFields {
                if let item = textFields[0].text {
                    print(item)
                    self.modules.append(item)
                    print(self.modules)
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                    //this calls the function saveLIst
                    self.saveList()
                }
            }
        }))
        //This will cancel any input from the user when adding module
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        
        present(alert, animated: true, completion: nil)
    }
    //This a function to save your modules
    func saveList() {
        let savedModules = UserDefaults.standard
        savedModules.set(modules, forKey: "modules")
        savedModules.synchronize()
        print("module saved")
    }
    //This is to load the modules after saving
    override func viewDidLoad() {
        super.viewDidLoad()
        let savedModules = UserDefaults.standard
        if let loadedModules:[String] = savedModules.object(forKey: "modules") as! [String]? {
            print("data loaded")
            print(loadedModules)
            self.modules=loadedModules
            DispatchQueue.main.async{
                self.tableView.reloadData()
            }
            if let id:String = self.moduleID{
                print("view did load with module \(id)")
            }
        }
    }
    //this is to set a moduleID
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showModule"{
            print("segue with \(segue.identifier) identifier triggered")
            if let indexPath = self.tableView.indexPathForSelectedRow{
                print("found row \(indexPath.row)")
                if let navigationController = segue.destination as? UINavigationController{
                    if let moduleController = navigationController.topViewController as? ModuleController{
                        print("found module controller")
                        moduleController.moduleID = String(indexPath.row)
                    }
                }
            }
        }
    }
        

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    
    //this returns number of sections in this case 1
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    //This returns the length of the table view
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.modules.count
    }

    //This is to set a reuse identifier called "Modules"
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Modules", for: indexPath)

        if let label = cell.textLabel{
            label.text = self.modules[indexPath.row]
        }
        return cell
    }
    

    
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    

    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            modules.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            self.saveList()

        }    
    }
    

    
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
        let module:String = modules[fromIndexPath.row]
        self.modules.remove(at: fromIndexPath.row)
        modules.insert(module, at: to.row)
        self.tableView.reloadData()

    }
    

    
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
