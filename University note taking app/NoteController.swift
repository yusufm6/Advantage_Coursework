//
//  NoteController.swift
//  University note taking app
//
//  Created by Mohammad Yusuf on 28/11/2016.
//  Copyright © 2016 Mohammad Yusuf. All rights reserved.
//

import UIKit
//create a variable called myNote
var myNote:[String] = []
// create a protocol called Update delegate which will update any changes made to Note
protocol UpdateDelegate {
    func update(with note: Note, at index: Int)
}
//A NoteController class was created with its superclass UIViewController, UITextFieldDelegate, UITextViewDelegate
class NoteController: UIViewController, UITextFieldDelegate, UITextViewDelegate {
    //create a public variable called noteID
    public var noteID:Int?
    // create called delegate and set it as an optional just in case it does not update
    var delegate: UpdateDelegate?
    // create an outlet variable called noteField
    @IBOutlet weak var noteField: UITextView!
    // create an outlet variable called doneButton
    @IBOutlet weak var doneButton: UIBarButtonItem!
    //Clicking on the Done button will save your notes and hide the keyboard
    @IBAction func DoneButton(_ sender: AnyObject) {
        self.noteField.resignFirstResponder()
        self.saveNote()
    }
    
    //This is used to view delegates
    override func viewDidLoad() {
        super.viewDidLoad()
        self.doneButton.tintColor = UIColor.clear
        self.noteField.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: .UIKeyboardWillHide, object: nil)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //This is to show the keyboard
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    //This is tmake the return button on the simulator functional
    func textFieldShouldReturn(_ noteField: UITextField) -> Bool {
        noteField.resignFirstResponder()
        
        return true
    }
    //This is the function to show the keyboard
    func keyboardWillShow(_ notification: NSNotification){
        print("keyboard will show")
        self.doneButton.tintColor = nil
    }
    // This is the function to hide the keyboard
    func keyboardWillHide(_ notification: NSNotification){
    print("keyboard will hide")
    self.doneButton.tintColor = UIColor.clear
    }
    //This is to save any changes when writing any notes
    func saveNote(){
        let note:Note = Note(title: "", text: noteField.text)
        if let id:Int = self.noteID{
            print("saving note with id: \(id)")
            delegate?.update(with: note, at: id)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
