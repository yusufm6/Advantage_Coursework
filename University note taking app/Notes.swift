//
//  Notes.swift
//  University note taking app
//
//  Created by Mohammad Yusuf on 01/12/2016.
//  Copyright © 2016 Mohammad Yusuf. All rights reserved.
//

import Foundation

//This will throw errors if any of these conditions is true
enum NoteError : Error{
    case emptyString
    case duplicateItem
    case outOfRange(index : Int)
}


//Create a struct called Module
struct Note{
    var title: String
    var text: String
}

//Create a class called Modules
class Notes {
    var notes:[Note]
    
    public static let sharedInstance = Notes()
    private init(){
        self.notes = []
    }
    
    
    
    
    // add a note to the end of the list
    public func add(note: Note) {
        self.notes.append(note)
        
    }
    
    
    //This will throw an error if the selected module is out of range
    public func getNote(forModule module:String, atIndex index: Int) throws -> Note{
        if (index < 0) || (index > (self.notes.count - 1)){
            throw NoteError.outOfRange(index: index)
        }
        // get module from the array 'index' (look at my notes)
        return self.notes[index]
        
    }
    
    //return the length of the note array
    public var count: Int {
        get{
            return self.notes.count
        }
    }
    
    //This will remove all notes from the list
    public func clearList() {
        self.notes.removeAll()
    }
    
    
    //These are the singletons created with its functions insert, update, remove.
    public func insert(note: Note, at index: Int) throws {
        if (index < 0) || (index > (self.notes.count)){
            throw NoteError.outOfRange(index: index)
        }
        self.notes.insert(note, at: index)
    }
    
    public func update(note: Note, at index: Int) throws {
        if (index < 0) || (index > (self.notes.count - 1)){
            throw NoteError.outOfRange(index: index)
        }
        self.notes.remove(at: index)
        self.notes.insert(note, at: index)
        
    }
    
    public func remove(at index: Int) throws {
        if (index < 0) || (index > (self.notes.count - 1)){
            throw NoteError.outOfRange(index: index)
        }
        self.notes.remove(at: index)
        
    }
    
}
