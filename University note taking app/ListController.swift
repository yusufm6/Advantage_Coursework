//
//  ListController.swift
//  University note taking app
//
//  Created by Mohammad Yusuf on 28/11/2016.
//  Copyright © 2016 Mohammad Yusuf. All rights reserved.
//

import UIKit
//A ListController class has been created with UITableViewController and UpdateDelegate as the superclass
class ListController: UITableViewController, UpdateDelegate {
    //create a variable called notes
    var notes:[String] = []
    //This was first 'ctrl dragged' then added some code to perform the action in this case it will add notes and save it.
    @IBAction func AddNote(_ sender: AnyObject) {
        print("Note title added")
        let alert = UIAlertController(title: "New Note", message: "Enter Note Title Below", preferredStyle: .alert)
        alert.addTextField(configurationHandler: nil)
        alert.addAction(UIAlertAction(title: "Add", style: .default, handler: { (action) in
            if let textFields = alert.textFields {
                if let item = textFields[0].text {
                    print(item)
                    self.notes.append(item)
                    print(self.notes)
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                    self.saveList()
                }
            }
        }))
        //This will cancel any input from the user when adding a note title
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        
        present(alert, animated: true, completion: nil)
    }
    //This a function to save your notes
    func saveList() {
        let savedItems = UserDefaults.standard
        savedItems.set(notes, forKey: "notes")
        savedItems.synchronize()
        print("note title saved")
    }
    //This is to update the notes using the sharedInstance method
    func update(with note: Note, at index:Int){
        print("delegate method called with \(note.title) at index \(index)")
        try? Notes.sharedInstance.update(note: note, at: index)
        self.tableView.reloadData()
    }
    
    //This is to set a noteID as well as using delegate
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showModule"{
            print("segue with \(segue.identifier) identifier triggered")
            if let indexPath = self.tableView.indexPathForSelectedRow{
                print("found row \(indexPath.row)")
                if let navigationController = segue.destination as? UINavigationController{
                    if let noteController = navigationController.topViewController as? NoteController{
                        print("found module controller")
                        noteController.noteID = indexPath.row
                        noteController.delegate = self
                    }
                }
            }
        }
    }
//This is to load the notes after saving
    override func viewDidLoad() {
        super.viewDidLoad()
        try? Notes.sharedInstance.add(note: Note(title: "", text: ""))
        let savedNotes = UserDefaults.standard
        if let loadedNotes:[String] = savedNotes.object(forKey: "notes") as! [String]? {
            print("data loaded")
            print(loadedNotes)
            self.notes=loadedNotes
            DispatchQueue.main.async{
                self.tableView.reloadData()
            }
        }

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    //this returns number of sections in this case 1
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    //This returns the length of the table view
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.notes.count
    }

    //This is to set a reuse identifier called "Notes"
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Notes", for: indexPath)
        
        // Configure the cell...
        if let label = cell.textLabel{
            label.text = self.notes[indexPath.row]
        }
        return cell
    }
    

    
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    

    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            notes.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            self.saveList()
            
        }
    }
    

    
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
        let note:String = notes[fromIndexPath.row]
        self.notes.remove(at: fromIndexPath.row)
        notes.insert(note, at: to.row)
        self.tableView.reloadData()

    }
 

    
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
