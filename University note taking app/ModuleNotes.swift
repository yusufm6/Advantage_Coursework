//
//  ModuleNotes.swift
//  University note taking app
//
//  Created by Mohammad Yusuf on 01/12/2016.
//  Copyright © 2016 Mohammad Yusuf. All rights reserved.
//

import Foundation

//This will throw errors if any of these conditions is true
enum ModuleError : Error{
    case emptyString
    case duplicateItem
    case outOfRange(index : Int)
}

//Create a struct called Module
struct Module {
    var title: String
    var text: String
}

//Create a class called Modules
class Modules {
    //create a variable called modules where it will store all modules (from the struct)
    var modules:[Module]
    
    public static let sharedInstance = Modules()
    
    //This will set it as an empty array at the start
    private init() {
        self.modules = []
    }
    

    // add a module to the end of the list
    public func add(module: Module) {
        self.modules.append(module)
    
    }

//This will throw an error if the selected module is out of range
public func getModule(atIndex index: Int) throws -> Module{
    if (index < 0) || (index > (self.modules.count - 1)){
        throw ModuleError.outOfRange(index: index)
    }
    
    return self.modules[index]
    
}

//return the length of the module array
public var count: Int {
    get{
        return self.modules.count
    }
}

//This will remove all modules from the list
public func clearList() {
    self.modules.removeAll()
}

    
//These are the singletons created with its functions insert, update, remove.
public func insert(module: Module, at index: Int) throws {
    if (index < 0) || (index > (self.modules.count)){
        throw ModuleError.outOfRange(index: index)
    }
    self.modules.insert(module, at: index)
}

//Will throw an error if out of range
    public func update(module: Module, at index: Int) throws {
    if (index < 0) || (index > (self.modules.count - 1)){
        throw ModuleError.outOfRange(index: index)
    }
    self.modules.remove(at: index)
    self.modules.insert(module, at: index)
    
}

//Will throw an error if out of range
public func remove(at index: Int) throws {
    if (index < 0) || (index > (self.modules.count - 1)){
        throw ModuleError.outOfRange(index: index)
    }
    self.modules.remove(at: index)
    
}

}
